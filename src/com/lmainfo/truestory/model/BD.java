package com.lmainfo.truestory.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper
{
	public BD(Context context) {
		super(context, "db", null, 1);
		// TODO Auto-generated constructor stub
	}

	public void onCreate(SQLiteDatabase db)
	{
		//Cr�ation de la table historique
		db.execSQL("CREATE TABLE historiqueTrueStory (id INTEGER PRIMARY KEY AUTOINCREMENT, demande TEXT, reponse TEXT, temps FLOAT, date LONG, x FLOAT, y FLOAT);");
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		//Si on a effectu� une modification � la table, on les d�truit et on la recr�e
		db.execSQL("DROP TABLE if exists historiqueTrueStory");
		onCreate(db);
	}
	
	public Cursor getAnciennesRequetes(){
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor c = db.rawQuery("SELECT demande, reponse, temps, date, x, y  FROM historiqueTrueStory;", null );
		return c;
	}
	
	public void ajouterRequete(AncienneRequete ar){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues cv = new ContentValues();
		cv.put("demande", ar.getDemande());
		cv.put("reponse", ar.getReponse());
		cv.put("temps", ar.getTemps());
		cv.put("date", ar.getDate().getTime());
		cv.put("x", ar.getX());
		cv.put("y", ar.getY());
		db.insert("historiqueTrueStory", null, cv);
	}
	
	public void eraseBD(){
		this.onUpgrade(getWritableDatabase(), 0, 1);
	}
	
}