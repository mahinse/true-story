package com.lmainfo.truestory.model;

import java.util.Vector;

public class Historique {
	private Vector<AncienneRequete> ancienneRequetes;
	
	public Historique(){
		this.ancienneRequetes = new Vector<AncienneRequete>();
	}
	
	public Vector<AncienneRequete> getAncienneRequetes() {
		return ancienneRequetes;
	}
	
	public String[] getDemandesAncienneRequetes(){
		String[] noms = new String[this.getAncienneRequetes().size()];
		
		for(int i=0;i<noms.length;i++)
			noms[i] = this.getAncienneRequetes().get(i).getDemande();
		
		return noms;
	}
	
	public AncienneRequete getRequete(int position){
		return this.getAncienneRequetes().get(position);
	}

	public void setAncienneRequetes(Vector<AncienneRequete> ancienneRequetes) {
		this.ancienneRequetes = ancienneRequetes;
	}
	
	public void ajouterRequete(AncienneRequete ar){
		this.ancienneRequetes.add(ar);
	}
}
