package com.lmainfo.truestory.model;

import java.util.Date;

public class AncienneRequete {
	private String demande;
	private String reponse;
	private float temps;
	private Date date;
	private float x;
	private float y;
	
	public AncienneRequete(String demande, String reponse, float temps, Date date, float x, float y){
		this.demande = demande;
		this.reponse = reponse;
		this.temps = temps;
		this.date = date;
		this.x = x;
		this.y = y;
	}

	public String getDemande() {
		return demande;
	}

	public void setDemande(String demande) {
		this.demande = demande;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	public float getTemps() {
		return temps;
	}

	public void setTemps(float temps) {
		this.temps = temps;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	
}
