package com.lmainfo.truestory.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;

public class RetrieveAnswerFromLMA extends AsyncTask<String[],String[],String[]>{
	
	@Override
	protected String[] doInBackground(String[]... params) {
		HttpPost httppost;
        HttpResponse response;
        HttpClient httpclient;
        HttpEntity entity;
        List<NameValuePair> nameValuePairs;
        String retourBD;
        String[] reponseBD = null;

        try {
                httpclient = new DefaultHttpClient();
                httppost = new HttpPost(
                                "http://www.lmainformatique.ca/answerengine.php");
                // Ajout d'une variable POST dans la requ�te
                nameValuePairs = new ArrayList<NameValuePair>(1);
                nameValuePairs.add(new BasicNameValuePair("demande", params[0][0].trim()));
                nameValuePairs.add(new BasicNameValuePair("langage", params[0][1].trim()));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                // Execute HTTP Post Request
                response = httpclient.execute(httppost);
                entity = response.getEntity();
                retourBD = EntityUtils.toString(entity);
                reponseBD = retourBD.split("poulet");
        }

        catch (Exception e) {
                return new String[]{null,null};
        }


        return reponseBD;
	}
}
