package com.lmainfo.truestory.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;

import com.lmainfo.truestory.model.BD;
import com.lmainfo.truestory.model.AncienneRequete;
import com.lmainfo.truestory.model.Historique;
import com.lmainfo.truestory.model.RetrieveAnswerFromLMA;

public class Controlleur implements Parcelable{
	private BD bd;
	private Historique historique;
	private String langage;
	private AncienneRequete noMatchFound;
	
	public Controlleur(Parcel in) {
	    readFromParcel(in);
	}
	
	public Controlleur(Activity activity, Context context){
		//On regarde si nous avons acc�s � internet, sinon on ferme l'application
		if(isNetworkAvailable(context)){
			bd = new BD(context);
			historique = new Historique();
			langage = "en";
			noMatchFound = new AncienneRequete("", "", Float.parseFloat("0"), new Date(Calendar.getInstance().getTimeInMillis()), Float.parseFloat("0"), Float.parseFloat("0"));
			//langage = Locale.getDefault().getLanguage();
			Cursor c = bd.getAnciennesRequetes();
			while(c.moveToNext()){
				historique.ajouterRequete(new AncienneRequete(c.getString(0), c.getString(1), c.getFloat(2), new Date(c.getLong(3)), c.getFloat(4), c.getFloat(5)));
			}
			changementIntent(activity, "android.intent.action.RECON");
		}
	}
	
	public void changementIntent(Activity activity, String intent){
		Intent i = new Intent(intent);
		i.putExtra("controlleur", Controlleur.this);
		activity.startActivityForResult(i,1);
	}
	
	public void changementIntent(Activity activity, String intent, int position){
		Intent i = new Intent(intent);
		i.putExtra("controlleur", Controlleur.this);
		i.putExtra("position", position);
		activity.startActivityForResult(i,1);
	}
	
	public boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		boolean connected = networkInfo != null && networkInfo.isAvailable() &&
		        networkInfo.isConnected();
		return connected;
	}
	
	
	public int getReponse(ArrayList<String> matches){
		String reponse = null;
		String demande = null;
		AsyncTask<String[], String[], String[]> reponseBD = null;
		int position = -1;
		
		try{
			RetrieveAnswerFromLMA dbPHP = new RetrieveAnswerFromLMA();
			//On recherche un match dans notre base de donn�es
			for(String demandeMatch: matches){
				reponseBD = dbPHP.execute(new String[]{demandeMatch, this.getLangage()});
				if(reponseBD.get()[0] != null){
					demande = reponseBD.get()[0];
					reponse = reponseBD.get()[1];
					break;
				}
			}
			
			//Si on trouve un match, on l'ajoute � notre liste et � la BD
			if(reponse != null && demande != null){
				AncienneRequete ar = new AncienneRequete(demande, reponse, Float.parseFloat("0"), new Date(Calendar.getInstance().getTimeInMillis()), Float.parseFloat("0"), Float.parseFloat("0"));
				historique.ajouterRequete(ar);
				bd.ajouterRequete(ar);
				position = 1;
			}
		}
		catch(Exception e){
			//S'il ne trouve pas de correspondance, il retourne un exception
			this.setNoMatchFound(new AncienneRequete(matches.get(0), "", Float.parseFloat("0"), new Date(Calendar.getInstance().getTimeInMillis()), Float.parseFloat("0"), Float.parseFloat("0")));
			return position;
		}
		return position;
	}
	
	public BD getBD() {
		return bd;
	}
	
	public void eraseBD(){
		this.getBD().eraseBD();
		this.getHistorique().setAncienneRequetes(new Vector<AncienneRequete>());
	}

	public Historique getHistorique() {
		return historique;
	}

	public void setHistorique(Historique historique) {
		this.historique = historique;
	}
	
	
	public String getLangage() {
		return langage;
	}

	public void setLangage(String langage) {
		this.langage = langage;
	}

	public AncienneRequete getNoMatchFound() {
		return noMatchFound;
	}

	public void setNoMatchFound(AncienneRequete noMatchFound) {
		this.noMatchFound = noMatchFound;
	}


	/****************************************************
	 * Passage de l'objet Controlleur � un autre intent
	 ****************************************************/
	
	public static final Parcelable.Creator<Controlleur> CREATOR = new  Parcelable.Creator<Controlleur>() {
	    public Controlleur createFromParcel(Parcel in) {
	        return new Controlleur (in);
	    }

	    @Override
	    public Controlleur[] newArray(int size) {
	        return new Controlleur[size];
	    }
	};


	@Override
	public int describeContents() {
	    return 0;
	}

	@Override
	public void writeToParcel(final Parcel out, int flags) {
		out.writeString(langage);
	    long val = SystemClock.elapsedRealtime();
	    out.writeLong(val);
	    put(val, bd);
	    val = val + 5;
	    out.writeLong(val);
	    put(val, historique);
	    val = val + 5;
	    out.writeLong(val);
	    put(val, noMatchFound);
	}

	private void readFromParcel(final Parcel in) {
		langage = in.readString();
		long val = in.readLong();
	    bd = (BD)getParcelable(val);
		val = in.readLong();
	    historique = (Historique)getParcelable(val);
	    val = in.readLong();
	    noMatchFound = (AncienneRequete)getParcelable(val);
	    
	}

	/////

	private static final HashMap<Long, Object> sControlleurMap = new HashMap<Long, Object>(3);

	synchronized private static void put(long key, final Object obj) {
		sControlleurMap.put(key, obj);
	}

	synchronized private static Object getParcelable(long key) {
	    return sControlleurMap.remove(key);
	}
}
