package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class SplashScreen extends Activity {
	Controlleur controlleur;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        controlleur = new Controlleur(this, this.getApplicationContext());
        //S'il n'y a pas de connection disponible, on ferme l'application en disant qu'il n'y a pas de connection Internet.
        if(!controlleur.isNetworkAvailable(this)){
	        AlertDialog ad = new AlertDialog.Builder(this).create();
			ad.setTitle("No Internet Connection...");
			ad.setMessage("There is no Internet connection available, you need a working Internet connection to be able to use this application.");
			ad.setIcon(R.drawable.ic_launcher);
			ad.setButton("OK", new DialogInterface.OnClickListener() {
			      public void onClick(DialogInterface dialog, int which) {
			    	  finish();
			       } });
			ad.show();
        }
        //Sinon, on ferme directement l'application lorsque l'utilisateur appuie sur le bouton back et revient sur le splashscreen
        else{
        	finish();
        }
    }
}
