package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Contribution extends Activity {
	Controlleur controlleur;
	Button buttonTrad, buttonHistory, buttonSettings, buttonAdd, buttonSend;
    EditText subject, emailtext;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribution);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        
        //On va chercher les boutons
        buttonTrad = (Button)findViewById(R.id.buttonTrad);
        buttonHistory = (Button)findViewById(R.id.buttonHistory);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        
        //On ajoute les écouteurs
        buttonTrad.setOnClickListener(new Ecouteur());
        buttonHistory.setOnClickListener(new Ecouteur());
        buttonSettings.setOnClickListener(new Ecouteur());
        buttonAdd.setOnClickListener(new Ecouteur());
        
        //On disable le bouton de l'activité ouverte présentement
        buttonAdd.setEnabled(false);
        
        buttonSend=(Button)findViewById(R.id.emailsendbutton);
        subject=(EditText)findViewById(R.id.emailsubject);
        emailtext=(EditText)findViewById(R.id.emailtext);
       
        buttonSend.setOnClickListener(new Ecouteur());
    }
    
    private class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == buttonTrad){
				controlleur.changementIntent(Contribution.this, "android.intent.action.RECON");
			}
			else if(v == buttonHistory){
				controlleur.changementIntent(Contribution.this, "android.intent.action.HISTORY");
			}
			else if(v == buttonSettings){
				controlleur.changementIntent(Contribution.this, "android.intent.action.SETTINGS");
			}
			else if(v == buttonAdd){
				controlleur.changementIntent(Contribution.this, "android.intent.action.ADD");
			}
			
			else if(v == buttonSend){
				final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String []{ "marc_antoine_hinse@hotmail.com"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject.getText());
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, emailtext.getText());
                Contribution.this.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
			}
		}
	
    }  
}
