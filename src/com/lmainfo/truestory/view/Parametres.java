package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.app.Activity;
import android.content.Intent;

public class Parametres extends Activity {
	Controlleur controlleur;
	Button buttonTrad, buttonHistory, buttonSettings, buttonAdd, buttonErase;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parametres);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        
        //On va chercher les boutons
        buttonTrad = (Button)findViewById(R.id.buttonTrad);
        buttonHistory = (Button)findViewById(R.id.buttonHistory);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        buttonErase = (Button)findViewById(R.id.effacerBD);
        
        //On ajoute les écouteurs
        buttonTrad.setOnClickListener(new Ecouteur());
        buttonHistory.setOnClickListener(new Ecouteur());
        buttonSettings.setOnClickListener(new Ecouteur());
        buttonAdd.setOnClickListener(new Ecouteur());
        
        //On disable le bouton de l'activité ouverte présentement
        buttonSettings.setEnabled(false);
        
        buttonErase.setOnClickListener(new Ecouteur());
    }
    
    private class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == buttonTrad){
				controlleur.changementIntent(Parametres.this, "android.intent.action.RECON");
			}
			else if(v == buttonHistory){
				controlleur.changementIntent(Parametres.this, "android.intent.action.HISTORY");
			}
			else if(v == buttonSettings){
				controlleur.changementIntent(Parametres.this, "android.intent.action.SETTINGS");
			}
			else if(v == buttonAdd){
				controlleur.changementIntent(Parametres.this, "android.intent.action.ADD");
			}
			else if(v == buttonErase){
				controlleur.eraseBD();
			}
		}
	
    } 
}
