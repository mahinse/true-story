package com.lmainfo.truestory.view;

import java.util.ArrayList;
import java.util.List;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ReconnaissanceVocale extends Activity {
	static final int REQUEST_CODE = 1234;
	Controlleur controlleur;
	Button buttonTrad, buttonHistory, buttonSettings, buttonAdd, buttonRecord;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconnaissance_vocale);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        
        //On va chercher les boutons
        buttonTrad = (Button)findViewById(R.id.buttonTrad);
        buttonHistory = (Button)findViewById(R.id.buttonHistory);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        
        //On ajoute les �couteurs
        buttonTrad.setOnClickListener(new Ecouteur());
        buttonHistory.setOnClickListener(new Ecouteur());
        buttonSettings.setOnClickListener(new Ecouteur());
        buttonAdd.setOnClickListener(new Ecouteur());
        
        //On disable le bouton de l'activit� ouverte pr�sentement
        buttonTrad.setEnabled(false);
        
        buttonRecord = (Button)findViewById(R.id.buttonRecord);
        // On d�sactive le bouton s'il n'y a pas de logiciel de reconnaissance vocale d'install�
        PackageManager pm = getPackageManager();
        List<ResolveInfo> activities = pm.queryIntentActivities(
                new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
        if (activities.size() == 0)
        {
        	buttonRecord.setEnabled(false);
        	buttonRecord.setText("Recognizer not present");
        }
    }
    
    public void recordButtonClicked(View v)
    {
        startVoiceRecognitionActivity();
    }
    
    private void startVoiceRecognitionActivity()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Let her talk this time");
        startActivityForResult(intent, REQUEST_CODE);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK)
        {
            // On rempli une liste de string que l'engine de reconnaissance vocale croit avoir entendu
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            
			//On appelle l'affichage de la r�ponse
			controlleur.changementIntent(ReconnaissanceVocale.this, "android.intent.action.REC_REP", controlleur.getReponse(matches));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    private class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == buttonTrad){
				controlleur.changementIntent(ReconnaissanceVocale.this, "android.intent.action.RECON");
			}
			else if(v == buttonHistory){
				controlleur.changementIntent(ReconnaissanceVocale.this, "android.intent.action.HISTORY");
			}
			else if(v == buttonSettings){
				controlleur.changementIntent(ReconnaissanceVocale.this, "android.intent.action.SETTINGS");
			}
			else if(v == buttonAdd){
				controlleur.changementIntent(ReconnaissanceVocale.this, "android.intent.action.ADD");
			}
		}
	
    }

}

