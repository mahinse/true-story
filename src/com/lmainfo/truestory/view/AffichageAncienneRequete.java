package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;
import com.lmainfo.truestory.model.AncienneRequete;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;
import android.content.Intent;

public class AffichageAncienneRequete extends Activity {
	Controlleur controlleur;
	int positionAncienneRequete;
	AncienneRequete ancienneRequete;
	TextView demandeText, reponseText, tempsText, dateText;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affichage_ancienne_requete);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        positionAncienneRequete = i.getExtras().getInt("position");
        
        ancienneRequete = controlleur.getHistorique().getRequete(positionAncienneRequete);
        
        demandeText = (TextView)findViewById(R.id.demande);
        reponseText = (TextView)findViewById(R.id.reponse);
        tempsText = (TextView)findViewById(R.id.temps);
        dateText = (TextView)findViewById(R.id.date);
        
        demandeText.setText(ancienneRequete.getDemande());
        reponseText.setText(ancienneRequete.getReponse());
        tempsText.setText(ancienneRequete.getTemps()+"");
        dateText.setText(ancienneRequete.getDate()+"");
    }

}
