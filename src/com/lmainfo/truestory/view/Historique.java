package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class Historique extends ListActivity {
	Controlleur controlleur;
	Button buttonTrad, buttonHistory, buttonSettings, buttonAdd;
	ListView listHistory;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historique);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        
        //On va chercher les boutons
        buttonTrad = (Button)findViewById(R.id.buttonTrad);
        buttonHistory = (Button)findViewById(R.id.buttonHistory);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        
        //On ajoute les �couteurs
        buttonTrad.setOnClickListener(new Ecouteur());
        buttonHistory.setOnClickListener(new Ecouteur());
        buttonSettings.setOnClickListener(new Ecouteur());
        buttonAdd.setOnClickListener(new Ecouteur());
        
        //On disable le bouton de l'activit� ouverte pr�sentement
        buttonHistory.setEnabled(false);
        
        //On cr�e l'�couteur pour la liste
        setListAdapter( new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, controlleur.getHistorique().getDemandesAncienneRequetes() ));
        
        //Un seul choix est admis dans la liste et nous ajoutons un �couteur
        ListView list = getListView();
        list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        list.setTextFilterEnabled(true);
        list.setOnItemClickListener(new Gestion());
    }
    
    private class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == buttonTrad){
				controlleur.changementIntent(Historique.this, "android.intent.action.RECON");
			}
			else if(v == buttonHistory){
				controlleur.changementIntent(Historique.this, "android.intent.action.HISTORY");
			}
			else if(v == buttonSettings){
				controlleur.changementIntent(Historique.this, "android.intent.action.SETTINGS");
			}
			else if(v == buttonAdd){
				controlleur.changementIntent(Historique.this, "android.intent.action.ADD");
			}
		}
	
    }
    
    public class Gestion implements OnItemClickListener{
	    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	          //On appelle l'intention de l'ancienne requ�te
	          controlleur.changementIntent(Historique.this, "android.intent.action.AFF_ANC_REQ", position);
	        }
    }
    
}
