package com.lmainfo.truestory.view;

import com.lmainfo.truestory.R;
import com.lmainfo.truestory.controller.Controlleur;
import com.lmainfo.truestory.model.AncienneRequete;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Reconnaissance_Reponse extends Activity {
	Controlleur controlleur;
	Button buttonTrad, buttonHistory, buttonSettings, buttonAdd;
	TextView labelDemande, textDemande, labelReponse, textReponse;
	int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reconnaissance__reponse);
        
        Intent i = getIntent();
        controlleur = (Controlleur) i.getParcelableExtra("controlleur");
        position = i.getExtras().getInt("position");
        
        
        //On va chercher les TextView
        labelDemande = (TextView)findViewById(R.id.labelDemande);
    	labelReponse = (TextView)findViewById(R.id.labelReponse);
    	textDemande = (TextView)findViewById(R.id.textDemande);
    	textReponse = (TextView)findViewById(R.id.textReponse);
    	
        if(position == 1){
        	AncienneRequete dernierElement = controlleur.getHistorique().getAncienneRequetes().lastElement();
        	textDemande.setText(dernierElement.getDemande());
        	textReponse.setText(dernierElement.getReponse());
        }
        else{
        	AncienneRequete noMatchFound = controlleur.getNoMatchFound();
        	textDemande.setText(noMatchFound.getDemande());
        	labelReponse.setText("No match has been found. You're on your own buddy. But if you think it should be in the application, feel free to offer it in the Add tab!");
        	textReponse.setVisibility(View.INVISIBLE);
        }
        
        
      //On va chercher les boutons
        buttonTrad = (Button)findViewById(R.id.buttonTrad);
        buttonHistory = (Button)findViewById(R.id.buttonHistory);
        buttonSettings = (Button)findViewById(R.id.buttonSettings);
        buttonAdd = (Button)findViewById(R.id.buttonAdd);
        
        //On ajoute les écouteurs
        buttonTrad.setOnClickListener(new Ecouteur());
        buttonHistory.setOnClickListener(new Ecouteur());
        buttonSettings.setOnClickListener(new Ecouteur());
        buttonAdd.setOnClickListener(new Ecouteur());
        
    }
    
    @Override
    public void onBackPressed() {
    	finish();
    }
    
    private class Ecouteur implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v == buttonTrad){
				controlleur.changementIntent(Reconnaissance_Reponse.this, "android.intent.action.RECON");
			}
			else if(v == buttonHistory){
				controlleur.changementIntent(Reconnaissance_Reponse.this, "android.intent.action.HISTORY");
			}
			else if(v == buttonSettings){
				controlleur.changementIntent(Reconnaissance_Reponse.this, "android.intent.action.SETTINGS");
			}
			else if(v == buttonAdd){
				controlleur.changementIntent(Reconnaissance_Reponse.this, "android.intent.action.ADD");
			}
			finish();
		}
	
    }
}
